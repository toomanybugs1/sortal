/*
 * UserInfo.java
 * 
 * Copyright (c) 2012 Lolmewn <lolmewn@gmail.com>.
 * 
 * Sortal is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Sortal is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Sortal.  If not, see <http ://www.gnu.org/licenses/>.
 */
package nl.lolmewn.sortal;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Lolmewn <lolmewn@gmail.com>
 */
public class UserInfo {

    private final UUID uuid;

    private final HashMap<String, Integer> warpUses = new HashMap<>();
    private final HashMap<String, Integer> locUses = new HashMap<>();

    public UserInfo(UUID user) {
        this.uuid = user;
    }

    public boolean hasUsedWarp(String warp) {
        return this.warpUses.containsKey(warp);
    }

    public int getUsedWarp(String warp) {
        if (!this.warpUses.containsKey(warp)) {
            return 0;
        }
        return this.warpUses.get(warp);
    }

    public void addtoUsedWarp(String warp, int add) {
        if (!this.hasUsedWarp(warp)) {
            this.warpUses.put(warp, add);
            return;
        }
        this.warpUses.put(warp, this.warpUses.get(warp) + add);
    }

    public boolean hasUsedLocation(String loc) {
        return this.locUses.containsKey(loc);
    }

    public int getUsedLocation(String loc) {
        if (!this.hasUsedLocation(loc)) {
            return 0;
        }
        return this.locUses.get(loc);
    }

    public void addtoUsedLocation(String loc, int add) {
        if (!this.hasUsedLocation(loc)) {
            this.locUses.put(loc, add);
            return;
        }
        this.locUses.put(loc, this.locUses.get(loc) + add);
    }

    public String getUsername() {
        return Bukkit.getServer().getOfflinePlayer(this.uuid).getName();
    }

    public void save(MySQL m, String table) throws SQLException {
        Connection con = m.getConnection();
        PreparedStatement update = con.prepareStatement("UPDATE " + table + " SET used=? WHERE warp=? AND player_uuid=?");
        PreparedStatement insert = con.prepareStatement("INSERT INTO " + table + "(player_uuid, warp, used) VALUES (?, ?, ?)");
        for (String warp : this.warpUses.keySet()) {
            update.setInt(1, this.getUsedWarp(warp));
            update.setString(2, warp);
            update.setString(3, this.uuid.toString());
            if (update.executeUpdate() == 0) {
                insert.setString(1, this.uuid.toString());
                insert.setString(2, warp);
                insert.setInt(3, this.getUsedWarp(warp));
                insert.execute();
            }
        }
        update.close();
        insert.close();

        update = con.prepareStatement("UPDATE " + table + " SET used=? WHERE x=? AND y=? AND z=? AND world=? AND player_uuid=?");
        insert = con.prepareStatement("INSERT INTO " + table + " (player_uuid, x, y, z, world, used)");
        for (String loc : this.locUses.keySet()) {
            String[] split = loc.split(",");
            update.setInt(1, this.locUses.get(loc));
            update.setInt(2, Integer.parseInt(split[1]));
            update.setInt(3, Integer.parseInt(split[2]));
            update.setInt(4, Integer.parseInt(split[3]));
            update.setString(5, split[0]);
            update.setString(6, this.uuid.toString());
            if (update.executeUpdate() == 0) {
                insert.setString(1, this.uuid.toString());
                insert.setInt(2, Integer.parseInt(split[1]));
                insert.setInt(3, Integer.parseInt(split[2]));
                insert.setInt(4, Integer.parseInt(split[3]));
                insert.setString(5, split[0]);
                insert.setInt(6, this.locUses.get(loc));
            }
        }
        update.close();
        insert.close();

        con.close();

    }

    public void save(File f) {
        if (!f.exists()) {
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Bukkit.getLogger().log(Level.SEVERE, null, ex);
            }
        }
        YamlConfiguration c = YamlConfiguration.loadConfiguration(f);
        for (String warp : this.warpUses.keySet()) {
            c.set(this.uuid.toString() + "." + warp, this.getUsedWarp(warp));
        }
        for (String loc : this.locUses.keySet()) {
            c.set(this.uuid.toString() + "." + loc, this.getUsedLocation(loc));
        }
        try {
            c.save(f);
        } catch (IOException ex) {
            Bukkit.getLogger().log(Level.SEVERE, null, ex);
        }
    }

}
